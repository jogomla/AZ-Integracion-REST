package cl.az.assets.integracion.rest.exception;

public class ExternalServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6350386030996711689L;

	private static final int NO_CODE = -1;
	
	private final int errCode;

	public ExternalServiceException(String message) {
		super(message);
		errCode = NO_CODE;
	}
	
	public ExternalServiceException(int code, String message) {
		super(message);
		errCode = code;
	}

	public ExternalServiceException(String message, Throwable t) {
		super(message, t);
		errCode = NO_CODE;
	}
	
	public ExternalServiceException(int code, String message, Throwable t) {
		super(message, t);
		errCode = code;
	}
	
	public int getCode() {
		return this.errCode;
	}

}
