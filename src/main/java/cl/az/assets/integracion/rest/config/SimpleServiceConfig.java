package cl.az.assets.integracion.rest.config;

import java.util.Map;

/**
 * Clase para configuraci&oacute;n de clientes de servicios REST. S&oacute;lo
 * contiene el endpoint del servicio y flag para desactivar validaci&oacute;n de
 * certificados SSL
 */
public class SimpleServiceConfig {

	protected String endpoint;
	protected boolean sslDisabled;

	public SimpleServiceConfig() {
		super();
		sslDisabled = false;
	}

	public SimpleServiceConfig(String endpoint) {
		this.endpoint = endpoint;
		this.sslDisabled = false;
	}

	public SimpleServiceConfig(String endpoint, Map<String, String> headers) {
		this.endpoint = endpoint;
		this.sslDisabled = false;
	}

	public SimpleServiceConfig(String endpoint, Map<String, String> headers, boolean sslDisabled) {
		this.endpoint = endpoint;
		this.sslDisabled = sslDisabled;
	}

	public SimpleServiceConfig(String endpoint, boolean sslDisabled) {
		this.endpoint = endpoint;
		this.sslDisabled = sslDisabled;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public boolean isSslDisabled() {
		return sslDisabled;
	}

	public void setSslDisabled(boolean sslDisabled) {
		this.sslDisabled = sslDisabled;
	}

	@Override
	public String toString() {
		return "SimpleServiceConfig [endpoint=" + endpoint + ", sslDisabled=" + sslDisabled + "]";
	}

}
