package cl.az.assets.integracion.rest.exception;

public class SSLConfigException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5250776324097936409L;

	public SSLConfigException(String message) {
		super(message);
	}

	public SSLConfigException(String message, Throwable t) {
		super(message, t);
	}

}
