package cl.az.assets.integracion.rest.config;

/**
 * Clase para configuraci&oacute;n de clientes de servicios REST con soporte
 * para autenticaci&oacute;n b&aacute;sica
 */
public class BasicAuthServiceConfig extends SimpleServiceConfig {

	private static final String BLANK = "";

	private String username;
	private String password;

	public BasicAuthServiceConfig() {
		super();
		this.endpoint = BLANK;
		this.username = BLANK;
		this.password = BLANK;
	}

	public BasicAuthServiceConfig(String endpoint, String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.endpoint = endpoint;
	}

	public BasicAuthServiceConfig(String endpoint, String username, String password, boolean disableSSL) {
		super();
		this.username = username;
		this.password = password;
		this.endpoint = endpoint;
		this.sslDisabled = disableSSL;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "ServiceConfig [username=" + username + ", password=" + password + ", endpoint=" + endpoint + "]";
	}

}
