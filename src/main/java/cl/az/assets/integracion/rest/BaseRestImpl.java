package cl.az.assets.integracion.rest;

import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.codehaus.jackson.Base64Variants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import cl.az.assets.integracion.rest.config.BasicAuthServiceConfig;
import cl.az.assets.integracion.rest.config.SimpleServiceConfig;
import cl.az.assets.integracion.rest.exception.EntityConfigException;
import cl.az.assets.integracion.rest.exception.ExternalServiceException;
import cl.az.assets.integracion.rest.exception.NoAuthException;
import cl.az.assets.integracion.rest.exception.NoConfigException;
import cl.az.assets.integracion.rest.exception.SSLConfigException;

public abstract class BaseRestImpl {

	private static final String ERROR_CONFIG_SOLICITUD = "Error configurando solicitud a servicio";
	private static final String ERROR_EJEC_LLAMADA = "Error ejecutando solicitud a servicio";
	private static final String BLANK = "";
	
	@Autowired
	protected RestTemplate template;

	/**
	 * Desactiva verificaci&oacute;n SSL
	 */
	protected static void disableSslVerification() {
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			throw new SSLConfigException("Error algoritmo SSL", e);
		} catch (KeyManagementException e) {
			throw new SSLConfigException("Error de llaves SSL", e);
		}
	}

	/**
	 * Configuraci&oacute;n de headers para solicitudes REST. Utiliza por
	 * defecto cabecera <code>content-type</code> con valor
	 * <code>application/json</code>
	 * 
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig}
	 * @return Instancia de {@link HttpHeaders} configurada
	 */
	protected HttpHeaders configHeaders(SimpleServiceConfig config) {
		MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
		return this.configHeaders(config, mediaType, null);
	}

	/**
	 * Configuraci&oacute;n de headers para solicitudes REST
	 * 
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig}
	 * @param mediaType
	 *            Instancia de {@link MediaType} a utilizar para cabecera
	 *            <code>content-type</code>
	 * @param headersMap
	 *            {@link Map} de cabeceras a agregar a solicitud
	 * @return Instancia de {@link HttpHeaders} configurada
	 */
	protected HttpHeaders configHeaders(SimpleServiceConfig config, MediaType mediaType,
			Map<String, String> headersMap) {
		HttpHeaders headers = new HttpHeaders();
		if (mediaType != null) {
			headers.setContentType(mediaType);
		}
		List<MediaType> accept = new ArrayList<MediaType>();
		accept.add(mediaType);
		headers.setAccept(accept);
		headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));

		// Inyecta campos de header
		if (headersMap != null) {
			for (String k : headersMap.keySet()) {
				headers.set(k, (String) headersMap.get(k));
			}
		}

		if (config instanceof BasicAuthServiceConfig) {
			BasicAuthServiceConfig baConfig = (BasicAuthServiceConfig) config;
			// configurar autenticacion basica
			String usrPass = baConfig.getUsername() + ":" + baConfig.getPassword();
			Base64Variants.getDefaultVariant().encode(usrPass.getBytes());
			headers.set("Authorization", "Basic " + Base64Variants.getDefaultVariant().encode(usrPass.getBytes()));
		}

		return headers;
	}

	/**
	 * Configuraci&oacute;n de headers para solicitudes REST
	 * 
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig}
	 * @param headersMap
	 *            {@link Map} de cabeceras a agregar a solicitud
	 * @return Instancia de {@link HttpHeaders} configurada
	 */
	protected HttpHeaders configHeaders(SimpleServiceConfig config, Map<String, String> headersMap) {
		return configHeaders(config, null, headersMap);
	}

	/**
	 * Valida valores b&aacute;sicos de conexi&oacute;n a servicio est&eacute;n
	 * presentes.<br>
	 * Adicionalmente desactiva validaci&oacute;n de certificados SSL si
	 * corresponde
	 * 
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig}
	 */
	protected void checkConfig(SimpleServiceConfig config) {
		if (config instanceof BasicAuthServiceConfig) {
			BasicAuthServiceConfig baConfig = (BasicAuthServiceConfig) config;
			if (BLANK.equals(baConfig.getUsername()) || baConfig.getUsername() == null
					|| BLANK.equals(baConfig.getPassword()) || baConfig.getPassword() == null) {
				throw new NoAuthException("No se encontraron credenciales para ejecutar servicio");
			}
		}

		if (BLANK.equals(config.getEndpoint())) {
			throw new NoConfigException("Endpoint de servicio no definido");
		}

		if (config.isSslDisabled()) {
			disableSslVerification();
		}

	}

	/**
	 * Crea instancia de {@link HttpEntity} que ser&aacute; utilizada para hacer
	 * solicitud a servicio REST
	 * 
	 * @param headers
	 *            Cabeceras de solicitud a servicio
	 * @param body
	 *            Data a enviar a servicio
	 * @return Instancia de {@link HttpEntity} a utilizar en solicitud a
	 *         servicio REST
	 * @throws EntityConfigException
	 *             En caso de ocurrir un error al procesar <code>body</code>
	 */
	protected HttpEntity<Object> configEntity(HttpHeaders headers, Object body) throws EntityConfigException {

		HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);

		return entity;
	}

	/**
	 * Crea instancia de {@link HttpEntity} utilizando un {@link MultiValueMap}
	 * como body
	 * 
	 * @param headers
	 *            Instancia de {@link HttpHeaders}
	 * @param form
	 *            {@link MultiValueMap} a utilizar como cuerpo de mensaje
	 * @return Instancia de {@link HttpEntity} configurada
	 * @throws EntityConfigException
	 *             En caso de ocurrir alg&uacute;n error
	 */

	/**
	 * Ejecuta llamada POST a servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param request
	 *            Data a enviar a servidor
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @return Respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir errores durante la llamada
	 */
	protected <T> T post(SimpleServiceConfig config, Object request, Class<T> responseType)
			throws ExternalServiceException {
		checkConfig(config);

		HttpHeaders headers = configHeaders(config);

		HttpEntity<Object> entity = null;
		try {
			entity = configEntity(headers, request);
		} catch (EntityConfigException e) {
			throw new ExternalServiceException(ERROR_CONFIG_SOLICITUD, e);
		}

		try {
			return template.postForObject(config.getEndpoint(), entity, responseType);
		} catch (HttpStatusCodeException hsce) {
			throw new ExternalServiceException(hsce.getStatusCode().value(), ERROR_EJEC_LLAMADA, hsce);
		} catch (RestClientException e) {
			throw new ExternalServiceException(ERROR_EJEC_LLAMADA, e);
		}
	}

	/**
	 * Ejecuta POST de datos de formulario a servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado como retorno
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param form
	 *            Data de formulario a enviar a servidor
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @return Respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir errores durante la llamada
	 */
	protected <T> T postForm(SimpleServiceConfig config, MultiValueMap<String, String> form, Class<T> responseType)
			throws ExternalServiceException {
		return postForm(config, form, responseType, null);
	}

	/**
	 * Ejecuta llamada POST vía Form a servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado como retorno
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param form
	 *            Data a enviar a servidor
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @param headersMap
	 *            headers a agregar a solicitud
	 * @return Respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir errores durante la llamada
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected <T> T postForm(SimpleServiceConfig config, MultiValueMap<String, String> form, Class<T> responseType,
			MultiValueMap headersMap) throws ExternalServiceException {
		checkConfig(config);
		ResponseEntity<T> exchange = null;
		HttpEntity<Object> request = null;

		HttpHeaders headers = configHeaders(config, MediaType.APPLICATION_FORM_URLENCODED, headersMap);

		try {
			request = this.configEntity(headers, form);
		} catch (EntityConfigException e1) {
			throw new ExternalServiceException("Error en el servicio", e1);
		}

		exchange = template.exchange(config.getEndpoint(), HttpMethod.POST, request, responseType);

		return exchange.getBody();
	}

	/**
	 * Ejecuta llamada GET a servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @param parameters
	 *            instancia de {@link Map} con par&aacute;metros a enviar a
	 *            servicio
	 * @return Respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir errores durante la llamada
	 */
	protected <T> T get(SimpleServiceConfig config, Class<T> responseType, Map<String, Object> parameters)
			throws ExternalServiceException {
		checkConfig(config);

		try {
			if (parameters != null && !parameters.isEmpty()) {
				return template.getForObject(config.getEndpoint(), responseType, parameters);
			} else {
				return template.getForObject(config.getEndpoint(), responseType);
			}
		} catch (RestClientException e) {
			throw new ExternalServiceException(ERROR_EJEC_LLAMADA, e);
		}
	}

	/**
	 * Ejecuta llamada GET a servicio REST, configurando datos en cabecera de
	 * solicitud
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @param parameters
	 *            instancia de {@link Map} con par&aacute;metros a enviar a
	 *            servicio
	 * @param headers
	 *            cabeceras que se agregar&aacute;n a solicitud
	 * @return Respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir errores durante la llamada
	 */
	protected <T> T get(SimpleServiceConfig config, Class<T> responseType, Map<String, Object> parameters,
			Map<String, String> headers) throws ExternalServiceException {
		checkConfig(config);

		HttpHeaders httpHeaders = configHeaders(config, MediaType.APPLICATION_JSON, headers);
		HttpEntity<Object> entity = null;

		try {
			entity = configEntity(httpHeaders, null);
		} catch (EntityConfigException e) {
			throw new ExternalServiceException(ERROR_CONFIG_SOLICITUD, e);
		}

		try {
			ResponseEntity<T> response = null;
			if (parameters != null && !parameters.isEmpty()) {
				UriTemplate uriTemplate = new UriTemplate(config.getEndpoint());
				response = template.exchange(uriTemplate.expand(parameters), HttpMethod.GET, entity, responseType);
			} else {
				response = template.exchange(config.getEndpoint(), HttpMethod.GET, entity, responseType);
			}
			return response.getBody();
		} catch (RestClientException e) {
			throw new ExternalServiceException(ERROR_EJEC_LLAMADA, e);
		}
	}

	/**
	 * Ejecuta llamada PUT a servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param request
	 *            Data a enviar a servidor
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @param parameters
	 *            Par&aacute;metros de URL (puede ser vac&iacute;o o
	 *            <code>null</code>)
	 * @return Cuerpo de respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir error en llamada a servicio
	 */
	@SuppressWarnings("unchecked")
	protected <T> T put(SimpleServiceConfig config, Object request, Class<T> responseType,
			Map<String, Object> parameters) throws ExternalServiceException {
		checkConfig(config);

		HttpHeaders headers = configHeaders(config);

		HttpEntity<Object> entity = null;
		try {
			entity = configEntity(headers, request);
		} catch (EntityConfigException e) {
			throw new ExternalServiceException(ERROR_CONFIG_SOLICITUD, e);
		}
		ResponseEntity<Object> response = null;
		if (parameters != null && !parameters.isEmpty()) {
			response = (ResponseEntity<Object>) template.exchange(config.getEndpoint(), HttpMethod.PUT, entity,
					responseType, parameters);
		} else {
			response = (ResponseEntity<Object>) template.exchange(config.getEndpoint(), HttpMethod.PUT, entity,
					responseType);
		}

		return (T) response.getBody();
	}

	/**
	 * Ejecuta llamada PUT a servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param request
	 *            Data a enviar a servidor
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @return Cuerpo de respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir error en llamada a servicio
	 */
	protected <T> T put(SimpleServiceConfig config, Object request, Class<T> responseType)
			throws ExternalServiceException {

		return put(config, request, responseType, null);
	}

	/**
	 * Ejecuta llamada DELETE hacia servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @param parameters
	 *            Par&aacute;metros de URL (puede ser vac&iacute;o o
	 *            <code>null</code>)
	 * @return Cuerpo de respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir error en llamada a servicio
	 */
	@SuppressWarnings("unchecked")
	protected <T> T delete(SimpleServiceConfig config, Class<T> responseType, Map<String, Object> parameters)
			throws ExternalServiceException {
		checkConfig(config);

		ResponseEntity<Object> response = null;
		if (parameters != null && !parameters.isEmpty()) {
			response = (ResponseEntity<Object>) template.exchange(config.getEndpoint(), HttpMethod.DELETE, null,
					responseType, parameters);
		} else {
			response = (ResponseEntity<Object>) template.exchange(config.getEndpoint(), HttpMethod.DELETE, null,
					responseType);
		}

		return (T) response.getBody();
	}

	/**
	 * Ejecuta llamada DELETE hacia servicio REST
	 * 
	 * @param <T>
	 *            Tipo esperado
	 * @param config
	 *            Instancia de {@link SimpleServiceConfig} con data requerida
	 *            para configurar llamada a servicio
	 * @param responseType
	 *            Tipo esperado como respuesta
	 * @return Cuerpo de respuesta obtenida desde servicio
	 * @throws ExternalServiceException
	 *             En caso de ocurrir error en llamada a servicio
	 */
	protected <T> T delete(SimpleServiceConfig config, Class<T> responseType) throws ExternalServiceException {
		return delete(config, responseType, null);
	}

}
