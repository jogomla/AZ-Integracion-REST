package cl.az.assets.integracion.rest.exception;

public class NoConfigException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -9203880490374110414L;

	public NoConfigException(String message) {
		super(message);
	}

	public NoConfigException(String message, Throwable t) {
		super(message, t);
	}

}
