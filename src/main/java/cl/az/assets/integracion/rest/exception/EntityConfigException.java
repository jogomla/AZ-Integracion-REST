package cl.az.assets.integracion.rest.exception;

public class EntityConfigException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1166017515408707679L;

	public EntityConfigException(String message) {
		super(message);
	}

	public EntityConfigException(String message, Throwable t) {
		super(message, t);
	}

}
