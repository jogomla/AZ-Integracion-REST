package cl.az.assets.integracion.rest.config.env;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import cl.az.assets.integracion.rest.util.CustomErrorHandler;

@Configuration
public class RestTemplateConfiguration {

	//@Value("${integracion.base.rest.timeout.connect:0}")
	private int connectionTimeout = 0;
	///@Value("${integracion.base.rest.timeout.read:0}")
	private int readTimeout = 0;

	@Bean
	public RestTemplate template() {
		SimpleClientHttpRequestFactory rf = new SimpleClientHttpRequestFactory();
		rf.setConnectTimeout(connectionTimeout);
		rf.setReadTimeout(readTimeout);
		RestTemplate template = new RestTemplate();
		template.setErrorHandler(new CustomErrorHandler());
		return template;
	}

}
