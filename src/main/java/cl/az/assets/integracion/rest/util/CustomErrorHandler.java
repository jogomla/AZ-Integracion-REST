package cl.az.assets.integracion.rest.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;

public class CustomErrorHandler implements ResponseErrorHandler {

	private static final Log LOG = LogFactory.getLog(CustomErrorHandler.class);

	/**
	 * Determina si ocurri&oacute; un error en llamada a Servicio
	 */
	public boolean hasError(ClientHttpResponse response) throws IOException {
		boolean error = false;
		LOG.info("resp content-type: " + response.getHeaders().getContentType());
		try {
			error = response.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR
					|| response.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR;

		} catch (Exception e) {
			LOG.warn("Error intentando procesar data: " + e.getMessage());
			throw new IOException("Error intentando procesar llamada a servicio", e);
		}
		return error;
	}

	public void handleError(ClientHttpResponse response) throws IOException {
		HttpStatus status = null;
		String statusText = null;
		String body = null;
		try {
			status = response.getStatusCode();
			statusText = response.getStatusText();
			body = readBody(response.getBody());
			LOG.debug("Cuerpo respuesta: " + body);
		} catch (Exception e) {
			LOG.warn("Error intentando procesar data: " + e.getMessage());
			throw new IOException("No se pudo procesar error", e);
		}
		if (status.series() == HttpStatus.Series.CLIENT_ERROR) {
			throw new HttpClientErrorException(status, statusText, body.getBytes(), StandardCharsets.UTF_8);
		} else {
			throw new HttpServerErrorException(status, statusText, body.getBytes(), StandardCharsets.UTF_8);
		}
	}

	/**
	 * Lee el cuerpo de una respuesta HTTP y la retorna como {@link String}.<br>
	 * Tiene el inconveniente de que se cierra el {@link InputStream} obtenido
	 * desde servicio, por lo que no se puede volver a leer.
	 * 
	 * @param is
	 *            {@link InputStream} obtenido desde respuesta de servicio
	 * @return {@link String} con el cuerpo de la respuesta obtenida
	 */
	private String readBody(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder body = new StringBuilder();
		String line;
		boolean flag = false;
		try {
			while ((line = reader.readLine()) != null) {
				body.append(flag ? System.getProperty("line.separator") : line);
				flag = true;
			}
		} catch (IOException e) {
			LOG.warn("Error procesando cuerpo de respuesta", e);
		}

		return body.toString();
	}

}
