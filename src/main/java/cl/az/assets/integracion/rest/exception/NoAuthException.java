package cl.az.assets.integracion.rest.exception;

public class NoAuthException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -236672344057999331L;

	public NoAuthException(String message) {
		super(message);
	}

	public NoAuthException(String message, Throwable t) {
		super(message, t);
	}

}
